  <div class="grid-wrapp">
                            <div class="quarter-final">
                                <div class="row">
                                    <div class="grid-item">
                                        <div class="head">
                                            <span class="category">Кудо</span>
                                        </div>
                                        <div class="body">
                                            <span class="name">Дем'ян Будза</span>
                                        </div>
                                        <div class="line"></div>
                                    </div>
                                    <div class="grid-item">
                                        <div class="head">
                                            <span class="category">Рукопашний бій</span>
                                        </div>
                                        <div class="body">
                                            <span class="name">Олександр Мухін</span>
                                        </div>
                                        <div class="line"></div>
                                    </div>
                                    <div class="line-vertical"></div>
                                    <div class="line-horizontal"></div>
                                    <a href="#" class="btn-play">
                                        <img src="/img/play.svg" alt="">
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="grid-item">
                                        <div class="head">
                                            <span class="category">Кікбоксинг</span>
                                        </div>
                                        <div class="body">
                                            <span class="name">Дмитро П'ятницький</span>
                                        </div>
                                        <div class="line"></div>
                                    </div>
                                    <div class="grid-item">
                                        <div class="head">
                                            <span class="category">К1</span>
                                        </div>
                                        <div class="body">
                                            <span class="name">Ніджат Валієв</span>
                                        </div>
                                        <div class="line"></div>
                                    </div>
                                    <div class="line-vertical"></div>
                                    <div class="line-horizontal"></div>
                                    <a href="#" class="btn-play">
                                        <img src="/img/play.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="semi-final">
                                <div class="row">
                                    <div class="grid-item">
                                        <div class="head">
                                            <span class="category"> </span>
                                        </div>
                                        <div class="body">
                                            <span class="name">Переможець першої пари</span>
                                        </div>
                                        <div class="line"></div>
                                    </div>
                                    <div class="grid-item">
                                        <div class="head">
                                            <span class="category"> </span>
                                        </div>
                                        <div class="body">
                                            <span class="name">Переможець другої пари </span>
                                        </div>
                                        <div class="line"></div>
                                    </div>
                                    <div class="line-vertical"></div>
                                    <div class="line-horizontal"></div>
                                    <a href="#" class="btn-play">
                                        <img src="/img/play.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="final">
                                <div class="row">
                                    <div class="grid-item">
                                        <div class="head">
                                            <span class="category"> </span>
                                        </div>
                                        <div class="body">
                                            <span class="name">Фiнал</span>
                                        </div>
                                        <div class="line"></div>
                                    </div>
                                    <div class="grid-cup">
                                        <img src="/img/trophy.svg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>