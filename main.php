<?php  
include("header.php");                     $link = mysqli_connect("localhost", "pb_fight_user", "yiduWtxe", "pb_fight_db") 
or die("Ошибка " . mysqli_error($link));

mysqli_query($link, "SET NAMES 'utf8'"); 
?>
<div class="section-main">
	<div class="main-description">
		<div class="container">
			<div class="logo-large">
				<img src="/img/pitbull-fight-logo.svg" alt="">
			</div>
			<div class="buttons-wrapp">
				<a href="#" class="btn btn-pink">
					<img class="arrow" src="/img/arrow.svg" alt="">
					Дивитись
					<img class="arrow" src="/img/arrow.svg" alt="">
				</a>
				<a href="#" class="btn btn-pink btn-circle">
					<img src="/img/youtube-white.svg" alt="">
				</a>
				<a href="#" class="btn btn-pink btn-circle">
					<img src="/img/instagram-white.svg" alt="">
				</a>
			</div>
		</div>
	</div>
	<div class="img-glitch">
		<div class="glitch"></div>
	</div>
</div>
<!-- <div class="section-videos section-grey">
	<div class="container">
		<div class="main-video">
			<?php 

			$query = "SELECT * FROM video LIMIT 1";
			$result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
			$main_video  = mysqli_fetch_row($result);
			echo '<div class="video">
			<iframe src="'.$main_video[2].'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>';
			?>

		</div>
		<div class="video-list">
			<?php 
			$videos_query = "SELECT * FROM video";
			$videos_result = mysqli_query($link, $videos_query) or die("Ошибка " . mysqli_error($link));
			while($video =mysqli_fetch_array($videos_result, MYSQLI_ASSOC)){
				$videos[] = $video;
			}
			foreach ( $videos as $video ) {
				echo '<div class="video-item">
				<div class="video">
				<iframe src="'.$video['link'].'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<div class="name">'.$video['title'].'</div>
				<div class="date">7 июня 2019</div>
				</div>';
			}

			?>
                </div>
                <div class="button-wrapp">
                	<a href="https://www.youtube.com/channel/UCs2phFvcrCe49aGtD5eXaEg" target="_blank" class="btn btn-pink text-black">Дивись більше відео на нашому Youtube</a>
                </div>
            </div>
        </div> -->
        <div class="section-about section-white">
        	<div class="container">
        		<div class="h-main h-pink">Про проект</div>
        		<div class="p-main p-black">
        			Pit Bull Fight - це турнір серед бійців різних стилів. У нас немає традиційних змагань і стандартних правил. Немає рейтингів і промоутерів. Ми самі створюємо правила і відбираємо до участі тих, кому є що «сказати», хто хоче довести силу свого «спорту»: кікбокс або тай, ушу або карате і тд. Ти з нами? Це закритий клуб.
        		</div>
        		<div class="instagram-list">
        			<div class="instagram-item">
        				<img src="/img/photo-1.png" alt="">
        			</div>
        			<div class="instagram-item">
        				<img src="/img/photo-2.png" alt="">
        			</div>
        			<div class="instagram-item">
        				<img src="/img/photo-3.png" alt="">
        			</div>
        			<div class="instagram-item">
        				<img src="/img/photo-4.png" alt="">
        			</div>
        		</div>
        		<div class="buttons-wrapp">
        			<a href="https://www.instagram.com/fightpitbull/"  target="_blank" class="btn btn-pink text-black">Більше фото в Instagram</a>
        			<a href="https://www.facebook.com/pitbulldrink/"  target="_blank" class="btn btn-pink text-black">Більше відео в Youtube</a>
        		</div>
        	</div>
        </div>
<!--         <div class="section-social_mini section-pink">
        	<div class="container">
        		<div class="h-social_mini">Підписуйся на наші соцмережі:</div>
        	 <div class="buttons-wrapp">
                    <a href="https://www.instagram.com/fightpitbull/"  target="_blank" class="btn btn-white btn-circle">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="#D9017A" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14.4805 0H5.51913C2.47588 0 0 2.476 0 5.51924V14.4806C0 17.524 2.47588 19.9999 5.51913 19.9999H14.4805C17.524 19.9999 19.9999 17.5239 19.9999 14.4806V5.51924C20 2.476 17.524 0 14.4805 0ZM18.2255 14.4806C18.2255 16.5455 16.5455 18.2254 14.4806 18.2254H5.51913C3.45433 18.2255 1.77449 16.5455 1.77449 14.4806V5.51924C1.77449 3.45445 3.45433 1.77449 5.51913 1.77449H14.4805C16.5454 1.77449 18.2254 3.45445 18.2254 5.51924V14.4806H18.2255Z"/>
                            <path d="M10.0001 4.84668C7.15848 4.84668 4.84668 7.15848 4.84668 10.0001C4.84668 12.8417 7.15848 15.1534 10.0001 15.1534C12.8418 15.1534 15.1536 12.8417 15.1536 10.0001C15.1536 7.15848 12.8418 4.84668 10.0001 4.84668ZM10.0001 13.3788C8.13705 13.3788 6.62117 11.8631 6.62117 10C6.62117 8.13681 8.13693 6.62105 10.0001 6.62105C11.8634 6.62105 13.3791 8.13681 13.3791 10C13.3791 11.8631 11.8632 13.3788 10.0001 13.3788Z" />
                            <path d="M15.3696 3.34204C15.0278 3.34204 14.6919 3.48045 14.4505 3.72296C14.2078 3.96429 14.0684 4.30026 14.0684 4.64333C14.0684 4.98533 14.208 5.32118 14.4505 5.5637C14.6918 5.80503 15.0278 5.94462 15.3696 5.94462C15.7127 5.94462 16.0475 5.80503 16.29 5.5637C16.5325 5.32118 16.6709 4.98522 16.6709 4.64333C16.6709 4.30026 16.5325 3.96429 16.29 3.72296C16.0487 3.48045 15.7127 3.34204 15.3696 3.34204Z" />
                        </svg>    
                    </a>
                    <a href="https://www.youtube.com/channel/UCs2phFvcrCe49aGtD5eXaEg" target="_blank" class="btn btn-white btn-circle">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="#D9017A" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19.15 4.45C18.6075 3.485 18.0188 3.3075 16.82 3.24C15.6225 3.15875 12.6112 3.125 10.0025 3.125C7.38875 3.125 4.37625 3.15875 3.18 3.23875C1.98375 3.3075 1.39375 3.48375 0.84625 4.45C0.2875 5.41375 0 7.07375 0 9.99625C0 9.99875 0 10 0 10C0 10.0025 0 10.0038 0 10.0038V10.0063C0 12.9163 0.2875 14.5887 0.84625 15.5425C1.39375 16.5075 1.9825 16.6825 3.17875 16.7638C4.37625 16.8338 7.38875 16.875 10.0025 16.875C12.6112 16.875 15.6225 16.8337 16.8212 16.765C18.02 16.6837 18.6087 16.5088 19.1513 15.5438C19.715 14.59 20 12.9175 20 10.0075C20 10.0075 20 10.0038 20 10.0013C20 10.0013 20 9.99875 20 9.9975C20 7.07375 19.715 5.41375 19.15 4.45ZM7.5 13.75V6.25L13.75 10L7.5 13.75Z" />
                        </svg>   
                    </a>
                    <a href="https://www.facebook.com/pitbulldrink/"  target="_blank"  class="btn btn-white btn-circle">
                        <svg width="22" height="22" viewBox="0 0 22 22" fill="#D9017A" xmlns="http://www.w3.org/2000/svg">
                            <path d="M16.499 0.00457747L13.6462 0C10.441 0 8.36976 2.12509 8.36976 5.41423V7.91055H5.50132C5.25345 7.91055 5.05273 8.1115 5.05273 8.35937V11.9763C5.05273 12.2241 5.25368 12.4249 5.50132 12.4249H8.36976V21.5514C8.36976 21.7993 8.57048 22 8.81835 22H12.5609C12.8087 22 13.0094 21.799 13.0094 21.5514V12.4249H16.3633C16.6112 12.4249 16.8119 12.2241 16.8119 11.9763L16.8133 8.35937C16.8133 8.24036 16.7659 8.12638 16.6819 8.04215C16.5979 7.95793 16.4835 7.91055 16.3645 7.91055H13.0094V5.79439C13.0094 4.77727 13.2518 4.26094 14.5767 4.26094L16.4986 4.26025C16.7462 4.26025 16.9469 4.0593 16.9469 3.81166V0.453169C16.9469 0.205757 16.7464 0.00503522 16.499 0.00457747Z" />
                        </svg>        
                    </a>
                </div>
        	</div>
        </div> -->
     <!--    <div class="section-event section-black noise">
        	<div class="container">
        		<div class="h-main h-pink">Як потрапити на подію?</div>
            <div class="step-list">
                    <div class="step-item">
                        <div class="icon">
                            <img src="/img/picture.svg" alt="">
                        </div>
                        <div class="text">
                           Опублікуй фото <br> в Instagram або Facebook
                        </div>
                        <div class="button-wrapp">
                            <a href="https://www.instagram.com/" class="btn btn-border">Опублікувати</a>
                        </div>
                    </div>
                    <div class="step-item">
                        <div class="icon">
                            <img src="/img/instagram-white.svg" alt="">
                        </div>
                        <div class="text">
                            Підпишись на  <span> @fightpitbull</span>
                        </div>
                        <div class="button-wrapp">
                            <a href="https://www.instagram.com/fightpitbull/" class="btn btn-border">Підписатися</a>
                        </div>
                    </div>
                    <div class="step-item">
                        <div class="icon">
                            <img src="/img/clipboard.svg" alt="">
                        </div>
                        <div class="text">
                           Заповни невелику форму
                        </div>
                        <div class="button-wrapp">
                            <a href="http://bit.ly/2Xo1TII" class="btn btn-border">Заповнити</a>
                        </div>
                    </div>
                </div>
        		<div class="more-info">
        			Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate, eius nihil quisquam a, consectetur nulla hic nostrum! Eum quis voluptas delectus facere optio doloribus sapiente. Harum ex dicta accusamus debitis incidunt consequuntur quos illo, voluptatem, facilis, minus deleniti voluptatum.
        		</div>
        	</div>
        </div> -->
        <div class="section-social_large section-pink top">
        	<div class="container">
        		<div class="h2-main">
        			Записи боїв, актуальні новини <br> та конкурси в наших соцмережах 
        		</div>
        		<div class="h-social_mini">Підписуйся на наші соцмережі:</div>
        		<div class="buttons-wrapp">
        			<a href="https://www.instagram.com/fightpitbull/" class="btn btn-white btn-circle">
        				<svg width="20" height="20" viewBox="0 0 20 20" fill="#D9017A" xmlns="http://www.w3.org/2000/svg">
        					<path d="M14.4805 0H5.51913C2.47588 0 0 2.476 0 5.51924V14.4806C0 17.524 2.47588 19.9999 5.51913 19.9999H14.4805C17.524 19.9999 19.9999 17.5239 19.9999 14.4806V5.51924C20 2.476 17.524 0 14.4805 0ZM18.2255 14.4806C18.2255 16.5455 16.5455 18.2254 14.4806 18.2254H5.51913C3.45433 18.2255 1.77449 16.5455 1.77449 14.4806V5.51924C1.77449 3.45445 3.45433 1.77449 5.51913 1.77449H14.4805C16.5454 1.77449 18.2254 3.45445 18.2254 5.51924V14.4806H18.2255Z"/>
        					<path d="M10.0001 4.84668C7.15848 4.84668 4.84668 7.15848 4.84668 10.0001C4.84668 12.8417 7.15848 15.1534 10.0001 15.1534C12.8418 15.1534 15.1536 12.8417 15.1536 10.0001C15.1536 7.15848 12.8418 4.84668 10.0001 4.84668ZM10.0001 13.3788C8.13705 13.3788 6.62117 11.8631 6.62117 10C6.62117 8.13681 8.13693 6.62105 10.0001 6.62105C11.8634 6.62105 13.3791 8.13681 13.3791 10C13.3791 11.8631 11.8632 13.3788 10.0001 13.3788Z" />
        					<path d="M15.3696 3.34204C15.0278 3.34204 14.6919 3.48045 14.4505 3.72296C14.2078 3.96429 14.0684 4.30026 14.0684 4.64333C14.0684 4.98533 14.208 5.32118 14.4505 5.5637C14.6918 5.80503 15.0278 5.94462 15.3696 5.94462C15.7127 5.94462 16.0475 5.80503 16.29 5.5637C16.5325 5.32118 16.6709 4.98522 16.6709 4.64333C16.6709 4.30026 16.5325 3.96429 16.29 3.72296C16.0487 3.48045 15.7127 3.34204 15.3696 3.34204Z" />
        				</svg>    
        			</a>
        			<a href="https://www.youtube.com/channel/UCs2phFvcrCe49aGtD5eXaEg" class="btn btn-white btn-circle">
        				<svg width="20" height="20" viewBox="0 0 20 20" fill="#D9017A" xmlns="http://www.w3.org/2000/svg">
        					<path d="M19.15 4.45C18.6075 3.485 18.0188 3.3075 16.82 3.24C15.6225 3.15875 12.6112 3.125 10.0025 3.125C7.38875 3.125 4.37625 3.15875 3.18 3.23875C1.98375 3.3075 1.39375 3.48375 0.84625 4.45C0.2875 5.41375 0 7.07375 0 9.99625C0 9.99875 0 10 0 10C0 10.0025 0 10.0038 0 10.0038V10.0063C0 12.9163 0.2875 14.5887 0.84625 15.5425C1.39375 16.5075 1.9825 16.6825 3.17875 16.7638C4.37625 16.8338 7.38875 16.875 10.0025 16.875C12.6112 16.875 15.6225 16.8337 16.8212 16.765C18.02 16.6837 18.6087 16.5088 19.1513 15.5438C19.715 14.59 20 12.9175 20 10.0075C20 10.0075 20 10.0038 20 10.0013C20 10.0013 20 9.99875 20 9.9975C20 7.07375 19.715 5.41375 19.15 4.45ZM7.5 13.75V6.25L13.75 10L7.5 13.75Z" />
        				</svg>   
        			</a>
        			<a href="https://www.facebook.com/pitbulldrink/" class="btn btn-white btn-circle">
        				<svg width="22" height="22" viewBox="0 0 22 22" fill="#D9017A" xmlns="http://www.w3.org/2000/svg">
        					<path d="M16.499 0.00457747L13.6462 0C10.441 0 8.36976 2.12509 8.36976 5.41423V7.91055H5.50132C5.25345 7.91055 5.05273 8.1115 5.05273 8.35937V11.9763C5.05273 12.2241 5.25368 12.4249 5.50132 12.4249H8.36976V21.5514C8.36976 21.7993 8.57048 22 8.81835 22H12.5609C12.8087 22 13.0094 21.799 13.0094 21.5514V12.4249H16.3633C16.6112 12.4249 16.8119 12.2241 16.8119 11.9763L16.8133 8.35937C16.8133 8.24036 16.7659 8.12638 16.6819 8.04215C16.5979 7.95793 16.4835 7.91055 16.3645 7.91055H13.0094V5.79439C13.0094 4.77727 13.2518 4.26094 14.5767 4.26094L16.4986 4.26025C16.7462 4.26025 16.9469 4.0593 16.9469 3.81166V0.453169C16.9469 0.205757 16.7464 0.00503522 16.499 0.00457747Z" />
        				</svg>        
        			</a>
        		</div>
        	</div>
        </div>
             <div class="section-event section-black noise top">
            <div class="container">
            <!--     <div class="h-main h-pink">Як потрапити на подію?</div>
             <div class="step-list">
                    <div class="step-item">
                        <div class="icon">
                            <img src="/img/picture.svg" alt="">
                        </div>
                        <div class="text">
                           Опублікуй фото <br> в Instagram або Facebook
                        </div>
                        <div class="button-wrapp">
                            <a href="https://www.instagram.com/" class="btn btn-border">Опублікувати</a>
                        </div>
                    </div>
                    <div class="step-item">
                        <div class="icon">
                            <img src="/img/instagram-white.svg" alt="">
                        </div>
                        <div class="text">
                            Підпишись на  <span> @fightpitbull</span>
                        </div>
                        <div class="button-wrapp">
                            <a href="https://www.instagram.com/fightpitbull/" class="btn btn-border">Підписатися</a>
                        </div>
                    </div>
                    <div class="step-item">
                        <div class="icon">
                            <img src="/img/clipboard.svg" alt="">
                        </div>
                        <div class="text">
                           Заповни невелику форму
                        </div>
                        <div class="button-wrapp">
                            <a href="http://bit.ly/2Xo1TII" class="btn btn-border">Заповнити</a>
                        </div>
                    </div>
                </div> -->
                <div class="more-info">
                     <a href="http://oddeeagency.com" target="_blank"><img src="/img/logo-1.png" alt="" /></a>
                </div>
            </div>
        </div>
    </div>

    <a class="link-product" target="_blank" href="https://pit-bull.ua ">
    	<img src="/img/botle.png" alt="">
    </a>

    <script src="js/main.js"></script>
    <script>
    	$(document).ready(function(){

    	})
    </script>
</body>
</html>