<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
           <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="/css/magnific-popup.css">
        <script src="/js/modernizr-3.7.1.min.js"></script>
        <!-- Place favicon.ico in the root directory -->
    </head>

    <body>

    <div class="wrapper">
        <div class="header">
            <div class="container">
                <div class="header-logo">
                    <a href="/" class="logo">
                        <img src="/img/pitbull-fight-logo.svg" alt="">
                    </a>
                </div>
                <span class="btn-navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
                <div class="header-nav">
                    <ul class="nav-list">
                        <li class="nav-item <?php if($_GET['p1']=='main') echo "active";?>">
                            <a href="/">Головна</a>
                        </li>
                        <li class="nav-item <?php if($_GET['p1']=='net') echo "active";?>">
                            <a href="/net/">Сітка боїв</a>
                        </li>
                        <li class="nav-item <?php if($_GET['p1']=='fighters') echo "active";?>">
                            <a href="/fighters/">Бійці</a>
                        </li>
                        <li class="nav-item <?php if($_GET['p1']=='arts') echo "active";?>">
                            <a href="/martial-arts/">Види єдиноборств</a>
                        </li>
                    </ul>
                    <!-- <div class="button-wrapp">
                        <a href="#" class="btn btn-pink">Як потрапити?</a>
                    </div> -->
                    <div class="social">
                        <a href="https://www.youtube.com/channel/UCs2phFvcrCe49aGtD5eXaEg" target="_blank" class="social-item"><img src="/img/youtube-white.svg" alt=""></a>
                        <a href="https://www.instagram.com/fightpitbull" target="_blank" class="social-item"><img src="/img/instagram-white.svg" alt=""></a>
                    </div>
                </div>
            </div>
        </div>